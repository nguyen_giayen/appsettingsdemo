﻿// See https://aka.ms/new-console-template for more information
using Demo;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO;

Console.WriteLine("Hello, World!");

//1. Create a service collection from DI
var serviceCollection= new ServiceCollection();

//2. Build a configuration
IConfiguration configuration;
configuration = new ConfigurationBuilder()
.SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
.AddJsonFile("appsettings.json")
.Build();


// 3. Add ther configuration to the service collection
serviceCollection.AddSingleton<IConfiguration>(configuration);
serviceCollection.AddSingleton<ITestClass>(new TestClass(configuration));

// Test
var serviceProvider = serviceCollection.BuildServiceProvider();
var test1 = serviceProvider.GetService<ITestClass>();
test1.TestMethod(); 




