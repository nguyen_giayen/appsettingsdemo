using Microsoft.Extensions.Configuration;

namespace Demo {

    public interface ITestClass
    {
         void TestMethod();
    }

    public class TestClass : ITestClass
    {
        private readonly IConfiguration _configuration;
        public TestClass(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void TestMethod ()
        {
            var data = _configuration.GetSection("DataValues:Name").Value;
            
            Console.WriteLine(data); 
            
        }

    }
  

}
